{-# OPTIONS --cubical --safe #-}

module test.notermination (A : Set) (_↝_ : A → A → Set) where

data Unit : Set where
  tt : Unit

data _↝*_ : A → A → Set where
  [] : ∀ {a} → (a ↝* a)
  _∷_ : ∀ {a b c} → (a ↝ b) → (b ↝* c) → (a ↝* c)

{-
-- does not termination check:
weird : ∀ {a b} (u : a ↝* b) → Unit
weird [] = tt
weird (s ∷ []) = tt
weird (s ∷ l@(t ∷ u)) = weird l
-}

{-
Evan Cavallo: Don't know about the cause of the failure, but this quick fix works for me:
-}

weird₁ : ∀ {a b} (u : a ↝* b) → Unit
weird₁ [] = tt
weird₁ (s ∷ u) = weird₁' s u
  where
  weird₁' : ∀ {a b c} (s : a ↝ b) (u : b ↝* c) → Unit
  weird₁' s [] = tt
  weird₁' s (t ∷ u) = weird₁' t u


-- weird (s ∷ (t ∷ u)) = weird (t ∷ u)

{- Message:
>  Termination checking failed for the following functions:
>    weird
>  Problematic calls:
>    weird (t ∷ u)

Agda version 2.6.2.1-59c7944
-}
