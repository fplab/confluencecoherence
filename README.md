## A Rewriting Coherence Theorem with Applications in Homotopy Type Theory

##### Nicolai Kraus and Jakob von Raumer

This repository contains an Agda formalisation of a rewriting argument in the
style of a construction by Craig C. Squier. This rewriting argument has
applications in homotopy type theory, as described in our paper with the same
name, published at MSCS. See
  doi:10.1017/S0960129523000026
and
  https://arxiv.org/abs/2107.01594.

The formalisation uses Agda 2.6.2.2 and relies on the cubical library (version
of Autumn 2022, commit 2131b6c08e32fdcf5b9292e5c6d6f23e4bf80fcd ). An html
version is availabe at: http://www.cs.nott.ac.uk/~psznk/agda/confluence/

The root file of the formalisation is index.agda, which imports everything.


#### MIT License

Copyright (c) 2022 Nicolai Kraus and Jakob von Raumer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


